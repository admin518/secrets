package convert

import (
	"crypto/sha256"
	"fmt"
	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
)

// CompareKey returns a string used to establish whether two issues are the same.
func CompareKey(file, fingerprint, ruleID string) string {
	if fingerprint != "" {
		return strings.Join([]string{file, fingerprint, ruleID}, ":")
	}
	return strings.Join([]string{file, ruleID}, ":")
}

// Fingerprint calculates the checksum of the reported source code.
func Fingerprint(sourceCode string) string {
	// Cleanup the source code extract from the report.
	sourceCode = strings.TrimSpace(sourceCode)
	// create code fingerprint using SHA256.
	h := sha256.New()
	h.Write([]byte(sourceCode))
	return fmt.Sprintf("%x", h.Sum(nil))
}

// Location returns a structured location.
func Location(file string, lineStart, lineEnd int) issue.Location {
	return issue.Location{
		File:      file,
		LineStart: lineStart,
		LineEnd:   lineEnd,
	}
}

// Identifiers returns the normalized identifiers of the issue.
func Identifiers(tool, ruleID string) []issue.Identifier {
	return []issue.Identifier{
		Identifier(tool, ruleID),
	}
}

// Identifier returns a structured Identifier for a issue.
func Identifier(tool, ruleID string) issue.Identifier {
	return issue.Identifier{
		Type:  issue.IdentifierType(fmt.Sprintf("%s_rule_id", strings.ToLower(tool))),
		Name:  fmt.Sprintf("%s rule ID %s", tool, ruleID),
		Value: ruleID,
	}
}
