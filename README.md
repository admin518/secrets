# Secrets analyzer

GitLab analyzer for leaked secrets.
This analyzer is based on the [Gitleaks](https://github.com/zricethezav/gitleaks),
and [TruffleHog](https://github.com/dxa4481/truffleHog) tools;
it reports possible secret leaks, like application tokens and cryptographic keys, in the source code and files contained
in your project.

This analyzer is written in Go using
the [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
shared by all analyzers.

The [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
contains documentation on how to run, test and modify this analyzer.

## Versioning and release process

Please check the common [Versioning and release process documentation](https://gitlab.com/gitlab-org/security-products/analyzers/common/blob/update_doc/README.md#versioning-and-release-process).

## Updating the underlying Scanners

This project adds a name and a description to the rules present in Gitleaks and TruffleHog; updating the
underlying tools requires updating this information.

To update a tool:
- Change its version in the [`Dockerfile`](Dockerfile).
- Update the map with new or updated rules (edit the tool's Go file in the [`convert`](convert) directory):
  - If new rules are present, add them.
  - If a rule has been renamed, add a new one to keep backward compatibility.
  - Ensure that Gitleaks and TruffleHog rules that detect the same thing (for example, an AWS key) have identical
    information, add those to the map in [`convert/common.go`](convert/common.go).

## Contributing

Contributions are welcome, see [`CONTRIBUTING.md`](CONTRIBUTING.md) for more details.

## License

This code is distributed under the GitLab Enterprise Edition (EE) license, see the [LICENSE](LICENSE) file.
