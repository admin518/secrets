# Secrets analyzer changelog

## v2.2.2
- Add check for env vars in password-in-url vulnerabilities

## v2.2.1
- Update common to v2.1.6

## v2.2.0
- Remove diffence (https://gitlab.com/gitlab-org/security-products/analyzers/secrets/merge_requests/13)

## v2.1.1
- Fix typos in reported messages

## v2.1.0
- Add support for generic api keys (https://gitlab.com/gitlab-org/gitlab/issues/10594)

## v2.0.5
- Set default severity value to Critical

## v2.0.4
- Fix: Set default Gitleaks entropy level to maximum to suppress false positives

## v2.0.3
- Fix: Update gitleaks file reader to better handle large files, fixes buffer overflow
- Fix: Update incorrect gitleaks rule RKCS8 to PKCS8

## v2.0.2
- Add gitleaks config, exclude svg analysis

## v2.0.1
- Fix gitleaks integration: don't parse output when there are no leaks

## v2.0.0
- Initial release
