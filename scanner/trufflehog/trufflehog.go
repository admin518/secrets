package trufflehog

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"net/url"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strings"
	"syscall"

	"github.com/logrusorgru/aurora"
	"github.com/urfave/cli"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
	"gitlab.com/gitlab-org/security-products/analyzers/secrets/v2/convert"
)

const pathTruffleHog = "trufflehog"

var PemBegin, PemEnd *regexp.Regexp

var issueScanner = issue.Scanner{
	ID:   "trufflehog",
	Name: "TruffleHog",
}

type Secret struct {
	StringsFound []string `json:"stringsFound"`
	Reason       string   `json:"reason"`
	Path         string   `json:"path"`
}

// Run runs TruffleHog on the path and transforms results into issues.
func Run(c *cli.Context, path string) ([]issue.Issue, error) {

	urlPath := "file://%s" + path
	cmd := exec.Command(pathTruffleHog, "--json", "--regex", "--entropy=False", urlPath)
	cmd.Env = os.Environ()
	cmd.Stderr = os.Stderr
	output, err := cmd.Output()

	// TruffleHog exits with code 1 when it finds issues.
	// See https://github.com/dxa4481/truffleHog/blob/2.0.97/truffleHog/truffleHog.py#L57
	if err == nil {
		return []issue.Issue{}, nil // no leaks
	}
	if exitErr, ok := err.(*exec.ExitError); ok {
		if exitErr.Sys().(syscall.WaitStatus).ExitStatus() == 1 {
			return toIssues(c.App.Writer, path, output) // leaks present
		}
	}
	fmt.Fprintln(c.App.Writer, aurora.Sprintf(aurora.Red("Couldn't run the trufflehog command: %v"), err))
	return nil, err
}

// toIssues reads a TruffleHog report, enrich it with line numbers and translate it into issues.
func toIssues(w io.Writer, path string, report []byte) ([]issue.Issue, error) {
	var (
		secrets []Secret
		issues  []issue.Issue
	)

	// The report is made of JSON values, one per line. Parse them line by line.
	lines := strings.Split(string(report), "\n")
	for _, line := range lines {
		if line == "" {
			continue
		}
		var secret = Secret{}
		if err := json.Unmarshal([]byte(line), &secret); err != nil {
			fmt.Fprintln(w, aurora.Sprintf(aurora.Red("Couldn't parse the TruffleHog report: %v"), err))
			return nil, err
		}
		secrets = append(secrets, secret)
	}

	// Translate to issues.
	SECRET:
	for _, secret := range secrets {
		var sourceCode string
		if len(secret.StringsFound) > 0 {
			sourceCode = secret.StringsFound[0]
		}
		filePath := secret.Path

		// Open the file to search it for the detected source code.
		f, err := os.Open(filepath.Join(path, filePath))
		if err != nil {
			fmt.Fprintln(w, aurora.Sprintf(aurora.Red("Couldn't open source file %s: %v"), filePath, err))
			return nil, err
		}

		// Search for the source code extract in each line of the file. Also detect end line for the PEM format.
		// TODO: extract function
		reader := bufio.NewReader(f)
		line := 1
		lineStart := 0
		lineEnd := 0
		isPem := PemBegin.MatchString(sourceCode)
		for {
			text, err := reader.ReadString('\n')

			if err == io.EOF {
				break
			} else if err != nil {
				fmt.Fprintln(w, aurora.Sprintf(aurora.Red("Problem while reading source file %s: %v"), filePath, err))
				return nil, err
			}

			if strings.Contains(text, sourceCode) {
				// Found the line
				lineStart = line
				lineEnd = line

				if secret.Reason == "Password in URL" {
					u, err := url.Parse(strings.Split(sourceCode, " ")[0])
					if err != nil {
						fmt.Fprintln(w, aurora.Sprintf(aurora.Red("Could not parse url: %v"), err))
						break
					}
					if u.User != nil {
						// Check if password is from an env var by checking if password used is
						// prefixed by "$". This does not guarantee the password is an env var but is likely
						pw, pwSet := u.User.Password()
						if pwSet && strings.HasPrefix(pw, "$") {
							if err := f.Close(); err != nil {
								fmt.Fprintln(w, aurora.Sprintf(aurora.Red("Problem while closing source file %s: %v"), filePath, err))
								return nil, err
							}
							continue SECRET
						}
					}
				}

				if !isPem {
					// No more work to do, this is not a multiline PEM secret.
					break
				}
			} else if PemEnd.MatchString(text) && isPem && lineStart != 0 {
				// End of the PEM block.
				lineEnd = line
				break
			}
			line++
		}
		if err := f.Close(); err != nil {
			fmt.Fprintln(w, aurora.Sprintf(aurora.Red("Problem while closing source file %s: %v"), filePath, err))
			return nil, err
		}

		// Compute Name and Description
		// TODO: extract function
		var name, description string
		rule, ruleFound := rules[secret.Reason]
		switch {
		case ruleFound:
			name = rule.Name
			description = rule.Description
		default:
			// This is an unknown rule. Warn the user and use default values.
			fmt.Fprintln(w,
				aurora.Sprintf(
					aurora.Red("No description for TruffleHog rule %s, please open an issue on https://gitlab.com/gitlab-org/gitlab/issues"),
					secret.Reason))

			name = fmt.Sprint("TruffleHog rule ", secret.Reason)
			description = fmt.Sprint("TruffleHog rule ", secret.Reason, " detected a secret")
		}

		// Append the issue.
		issues = append(issues, issue.Issue{
			Category:    issue.CategorySast,
			Scanner:     issueScanner,
			Name:        name,
			Message:     name,
			Description: description,
			CompareKey:  convert.CompareKey(filePath, convert.Fingerprint(sourceCode), secret.Reason),
			Severity:    issue.LevelCritical,
			Confidence:  issue.LevelUnknown,
			Location:    convert.Location(filePath, lineStart, lineEnd),
			Identifiers: convert.Identifiers("TruffleHog", secret.Reason),
		})
	}

	return issues, nil
}

func init() {
	PemBegin = regexp.MustCompile("-----BEGIN")
	PemEnd = regexp.MustCompile("-----END")
}
