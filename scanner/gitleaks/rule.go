package gitleaks

import "gitlab.com/gitlab-org/security-products/analyzers/secrets/v2/rule"

// See scanner/gitleaks/gitleaks.toml for a list of Gitleaks rules.
var rules = map[string]rule.Rule{
	"AWS":      rule.InfoAWS,
	"Facebook": rule.InfoFacebook,
	"Github":   rule.InfoGitHub,
	"PGP":      rule.InfoPGP,
	"PKCS8": {
		Name:        "PKCS8 key",
		Description: rule.SingleCredDesc("PKCS8 private key"),
	},
	"RSA":         rule.InfoRSA,
	"Slack token": rule.InfoSlack,
	"Stripe": {
		Name:        "Stripe",
		Description: rule.SingleCredDesc("Stripe API key"),
	},
	"SSH":             rule.InfoSSH,
	"Twitter":         rule.InfoTwitter,
	"Generic API Key": rule.InfoGenericAPI,
}
