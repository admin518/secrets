FROM golang:1.13 AS build
ENV CGO_ENABLED=0 GOOS=linux
WORKDIR /go/src/app
COPY . .
RUN go build -o analyzer

FROM alpine:3.9

ARG GITLEAKS_VERSION
ARG TRUFFLEHOG_VERSION

ENV GITLEAKS_VERSION ${GITLEAKS_VERSION:-v1.24.0}
ENV TRUFFLEHOG_VERSION ${TRUFFLEHOG_VERSION:-2.0.98}

RUN wget -O /usr/local/bin/gitleaks https://github.com/zricethezav/gitleaks/releases/download/${GITLEAKS_VERSION}/gitleaks-linux-amd64 && \
    chmod a+x /usr/local/bin/gitleaks

FROM python:3-alpine
RUN apk add --no-cache git && pip install gitdb2==3.0.0 trufflehog
    #apk add bash git py-pip && \
    # pip install truffleHog==${TRUFFLEHOG_VERSION}
    #pip install gitdb2==4.0.2 trufflehog

COPY --from=build --chown=root:root /go/src/app/analyzer /
COPY scanner/gitleaks/gitleaks.toml /gitleaks.toml

COPY --from=build --chown=root:root /go/src/app/analyzer /
COPY scanner/gitleaks/gitleaks.toml /gitleaks.toml

ENTRYPOINT []
CMD ["/analyzer", "run"]
