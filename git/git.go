package git

import (
	"io/ioutil"
	"os"
	"path/filepath"

	"github.com/otiai10/copy"
	"gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/plumbing/object"
)

// FlattenRepo flattens the given repository into a new directory containing only one commit and returns its path.
func FlattenRepo(path string) (string, error) {
	// Copy the repository to a new directory.
	flatPath, err := ioutil.TempDir("", "flat-")
	if err != nil {
		return "", err
	}
	if err = copy.Copy(path, flatPath); err != nil {
		return "", err
	}
	// Remove .git
	if err = os.RemoveAll(filepath.Join(flatPath, ".git")); err != nil {
		return "", err
	}
	// Init a new repository
	r, err := git.PlainInit(flatPath, false)
	if err != nil {
		return "", err
	}
	w, err := r.Worktree()
	if err != nil {
		return "", err
	}
	// Create a commit containing all the files.
	_, err = w.Add(".")
	if err != nil {
		return "", err
	}
	_, err = w.Commit("", &git.CommitOptions{
		Author: &object.Signature{
			Name:  "Analyzer",
			Email: "Analyzer",
		},
	})
	if err != nil {
		return "", err
	}

	return flatPath, nil
}
