package rule

import "fmt"

// Rule contain the name and description for a rule.
// TODO: extract solution from description
type Rule struct {
	Name        string
	Description string
}

func SingleCredDesc(subject string) string {
	return fmt.Sprintf("%s detected; please remove and revoke it if this is a leak.", subject)
}

func CredFileDesc(subject string) string {
	return fmt.Sprintf("%s found; it may contain credentials, please check and revoke them if this is a leak.", subject)
}

func SecretFileDesc(subject string) string {
	return fmt.Sprintf("%s found; it may contain secrets, please check and revoke what needs to be revoked if this is a leak.", subject)
}

var InfoAWS = Rule{
	Name:        "AWS API key",
	Description: SingleCredDesc("Amazon Web Services API key"),
}

var InfoFacebook = Rule{
	Name:        "Facebook token",
	Description: SingleCredDesc("Facebook token"),
}

var InfoGitHub = Rule{
	Name:        "GitHub token",
	Description: SingleCredDesc("GitHub token"),
}

var InfoPGP = Rule{
	Name:        "PGP private key",
	Description: SingleCredDesc("PGP private key"),
}

var InfoRSA = Rule{
	Name:        "RSA private key",
	Description: SingleCredDesc("RSA private key"),
}

var InfoSlack = Rule{
	Name:        "Slack token",
	Description: SingleCredDesc("Slack token"),
}

var InfoSSH = Rule{
	Name:        "SSH private key",
	Description: SingleCredDesc("SSH private key"),
}

var InfoTwitter = Rule{
	Name:        "Twitter key",
	Description: SingleCredDesc("Twitter key"),
}

var InfoGenericAPI = Rule{
	Name:        "Generic API Key",
	Description: SingleCredDesc("Unknown API key"),
}
